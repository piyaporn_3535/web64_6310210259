import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Center from './components/Center';

function App() {
  return (
    <div className="App">
      <Header />
      <Center />
      <Body />
      <Footer />
    </div>
  );
}

export default App;
